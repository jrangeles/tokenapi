package com.phase1.token.model;

public class Area {
	
	private Long id;
	private String areaName;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	
	public Area(Long id, String areaName) {
		super();
		this.id = id;
		this.areaName =areaName;
	}
	
}
