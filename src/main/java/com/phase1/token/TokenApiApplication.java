package com.phase1.token;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.phase1.token"})
@MapperScan("com.phase1.token.mapper")
public class TokenApiApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(TokenApiApplication.class, args);
	}

}
