package com.phase1.token.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.phase1.token.model.Area;

@Mapper
public interface AreaMapper {
	
	@Select("select * from jp_area")
	public List<Area> getAllArea();
	
}
