package com.phase1.token.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.phase1.token.model.Inquirer;

@Mapper
public interface InquirerMapper {

	@Select("SELECT * FROM inquirer")
	List<Inquirer> getInquirers();
	
	@Select("SELECT * FROM inquirer WHERE id = #{id}")
	@Results({
        @Result(property = "postalCode", column = "postal_code"),
        @Result(property = "phoneNumber", column = "phone_number")
    })
	Inquirer getInquirerById(@Param("id") Long id);
	
	@Insert("INSERT INTO inquirer (id, name, postal_code, address, phone_number, fax_number, mail_address, contents, url) Values(#{inquirer.id},"
			+ "#{inquirer.name}, #{inquirer.postalCode}, #{inquirer.address}, #{inquirer.phoneNumber}, #{inquirer.faxNumber}, #{inquirer.mailAddress}, #{inquirer.contents}, #{inquirer.url})")
	Integer insertInquirer(@Param("inquirer") Inquirer inquirer) throws Exception;
	
	@Update("UPDATE inquirer SET name = #{inquirer.getName()}")
	Inquirer updateInquiry(@Param("inquirer") Inquirer inquirer);
	
	@Delete("DELETE FROM inquirer WHERE id = #{id}")
	Integer deleteInquiryById(@Param("id") Long id);
	
}
