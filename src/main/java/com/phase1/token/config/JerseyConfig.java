package com.phase1.token.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import com.phase1.token.controller.AreaController;
import com.phase1.token.controller.InquirerController;

@Configuration
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig{
	
	private static final Logger logger = LoggerFactory.getLogger(JerseyConfig.class);
	
	public JerseyConfig() {
		register(AreaController.class);
		register(InquirerController.class);
		logger.info("Jersey was configured");
	}
}
