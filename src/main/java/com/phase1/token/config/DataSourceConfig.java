package com.phase1.token.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@ConfigurationProperties(prefix = "spring.datasource.hikari")
public class DataSourceConfig extends HikariConfig {

	@Bean(name= "hikariDataSource")
	public DataSource dataSource() throws SQLException {
		return new HikariDataSource(this);
	}
	
	@Bean(name="sqlSessionFactory")
    public SqlSessionFactory sqlTest1SessionFactory(@Qualifier("hikariDataSource") DataSource hikariDataSource, ApplicationContext applicationContext) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(hikariDataSource);
        return sqlSessionFactoryBean.getObject();
    }
	
	@Bean(name = "sqlSession")
    public SqlSessionTemplate sqlTest1SessionTemplate(SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
