package com.phase1.token.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.phase1.token.mapper.AreaMapper;


@Controller
@Path("/arealist")
public class AreaController {

	@Autowired
    private AreaMapper areaMapper;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response selectAllArea() {
		return Response.ok(areaMapper.getAllArea()).build();
	}
	
}
