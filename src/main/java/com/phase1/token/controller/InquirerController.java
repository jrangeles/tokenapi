package com.phase1.token.controller;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.phase1.token.config.JerseyConfig;
import com.phase1.token.mapper.InquirerMapper;
import com.phase1.token.model.Inquirer;

@Controller
@Path("/inquirer")
public class InquirerController {

	private static final Logger logger = LoggerFactory.getLogger(JerseyConfig.class);

	@Autowired
	private InquirerMapper inquirerMapper;

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInquirers() {
		logger.info("getInquirers");
		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		try {
			List<Inquirer> listInquirer = inquirerMapper.getInquirers();
			if (listInquirer == null) {
				response.put("inquirers", Collections.emptyMap());
			} else {
				response.put("total", listInquirer.size());
				response.put("inquirers", listInquirer);
			}
			return Response.status(Response.Status.OK).entity(listInquirer).build();
		} catch (Exception ex) {

			response.put("inquirers", "Not Found");
			return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
		}

	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInquirerById(@PathParam("id") Long id) {

		LinkedHashMap<String, Object> response = new LinkedHashMap<String, Object>();
		try {
			Inquirer inquirer = inquirerMapper.getInquirerById(id);
			if (inquirer == null) {
				response.put("inquirer", Collections.emptyMap());
			} else {
				response.put("inquirer", inquirer);
			}
			return Response.status(Response.Status.OK).entity(inquirer).build();
		} catch (Exception ex) {

			response.put("inquirer", "Not Found");
			return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
		}

	}

	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertInquirer(Inquirer inquirer) {
		LinkedHashMap<Object, Object> serviceResponse = new LinkedHashMap<Object, Object>();
		
		try {
			Integer createInquirer = inquirerMapper.insertInquirer(inquirer);

			if (createInquirer != 1) {
				serviceResponse.put("created", "unable to create user");
			} else {
				logger.info("Successfully created User.");
				serviceResponse.put("created_msg", "Successfully created User");
			}
			
			return Response.status(Response.Status.CREATED).entity(serviceResponse).build();
			
		} catch (Exception e) {
			logger.debug("<< create()");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(serviceResponse).build();
		}
	}

}
